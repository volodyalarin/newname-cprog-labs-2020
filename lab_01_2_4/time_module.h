#ifndef __TIME__MODULE__H__
#define __TIME__MODULE__H__

typedef struct
{
    unsigned hours;
    unsigned minutes;
    unsigned seconds;
} time_t;

unsigned long input_seconds();
void output_formated_time(const time_t *time);
time_t convert_time(unsigned long seconds);

#endif // __TIME__MODULE__H__