#include <stdio.h>
#include "time_module.h"

unsigned long input_seconds()
{
    unsigned long seconds; 

    printf("Input time in seconds: ");
    scanf("%lu", &seconds);

    return seconds;
}

void output_formated_time(const time_t *time)
{
    printf("%u %u %u", time->hours, time->minutes, time->seconds);
}

time_t convert_time(unsigned long seconds)
{
    time_t time;

    time.seconds = seconds % 60;
    time.minutes = seconds / 60 % 60;
    time.hours = seconds / 3600;
    
    return time;
}
