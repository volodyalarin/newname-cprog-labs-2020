/*
Задано время в секундах. Перевести в часы, минуты, секунды.
*/
#include "time_module.h"

int main()
{
    unsigned long seconds = input_seconds();

    time_t time = convert_time(seconds);

    output_formated_time(&time);
}