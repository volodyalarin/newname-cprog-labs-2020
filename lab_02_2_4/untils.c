#include <stdio.h>
#include "untils.h"

int input_array(long long *array, int *length)
{
    if (!array || !length)
        return POINTER_ERROR;
    
    int err = INPUT_SUCCESS;

    printf("Input length of array: ");
    if (scanf("%d", length) != 1 || *length <= 0 || *length > MAX_ARRAY_SIZE)
        err = INPUT_ERROR;
    if (!err)
        printf("Input array elements: ");
    
    for (int i = 0; i < (*length) && !err; i++)
    {
        if (scanf("%lld", array + i) != 1)
            err = INPUT_ERROR; 
    }
    
    return err;
}

int output_array(long long *array, int length)
{
    if (!array)
        return POINTER_ERROR;
    
    printf("Your array is \n");
    for (int i = 0; i < length; i++)
        printf("%lld ", array[i]);
    printf("\n");

    return 0;
}

int selection_sort(long long *array, const int length)
{
    if (!array)
        return POINTER_ERROR;
    
    for (int i = 0; i < length - 1; i++)
    {
        long long min_element = array[i];
        int min_position = i;
        
        for (int j = i + 1; j < length; j++)
        {
            if (min_element > array[j])
            {
                min_element = array[j];
                min_position = j;
            }            
        }

        array[min_position] = array[i];
        array[i] = min_element;
    }

    return 0;
}