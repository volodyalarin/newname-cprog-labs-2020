/*
Определить пересекаются ли два отрезка.
Ввод:
x1 y1 x2 y2 x3 y3 x4 y4 
Вывод:
0 (не пересекаются)
1 (пересекаются)
*/
#include <stdio.h>
#include "io_module.h"
#include "vectors.h"


int main()
{
    segment_t first_segment;
    segment_t second_segment;

    int err = input_segment(&first_segment);
    if (!err)
        err = input_segment(&second_segment);        

    switch (err)
    {
        case INPUT_SUCCESS:
            output_intersect(is_segments_intersect(&first_segment, &second_segment));
            break;
        case INPUT_ERROR:
            printf("Input error \n");
            break;
        case POINTER_ERROR:
            printf("Pointer error \n");
            break;
        default:
            printf("Undefined error \n");
    }

    return err;         
}