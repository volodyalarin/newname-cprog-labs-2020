#include <stdio.h>
#include "io_module.h"

int input_point(point_t *point)
{ 
    if (!point)
        return POINTER_ERROR;

    int err = INPUT_SUCCESS;   

    printf("Input x and y of the point: ");

    if (scanf("%lf %lf", &point->x, &point->y) != 2)
        err = INPUT_ERROR;
    
    return err;
}

int input_segment(segment_t *segment)
{
    if (!segment)
        return POINTER_ERROR;
       
    printf("Inputing segment \n");

    int err = input_point(&segment->begin);
    if (err == INPUT_SUCCESS)
        err = input_point(&segment->end);
    if (err == INPUT_SUCCESS && points_is_equal(&segment->begin, &segment->end))
        err = INPUT_ERROR;       

    return err;
}

void output_intersect(int is_intersect)
{
    if (is_intersect)
        printf("Intersects 1 \n");
    else
        printf("doesn't intersect 0 \n");
}