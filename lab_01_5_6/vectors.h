#ifndef __VECTORS__H__
#define __VECTORS__H__

#define EPS_FLOAT_EQUALS 1e-5

typedef struct
{
    double x;
    double y;
} point_t;

typedef struct
{
    double x;
    double y;
} vector_t;

typedef struct
{
    point_t begin;
    point_t end;    
} segment_t;

int points_is_equal(const point_t *first_point, const point_t *second_point);

segment_t create_segment(const point_t *begin_point, const point_t *end_point);

vector_t segment_to_vector(const segment_t *segment);
vector_t create_vector(const point_t *begin_point, const point_t *end_point);

double scalar_product(const vector_t *first_vector, const vector_t *second_vector);
double vector_product(const vector_t *first_vector, const vector_t *second_vector);

int is_belongs_to_segment(const point_t *point, const segment_t *segment);
int is_segments_intersect(const segment_t *first_segment, const segment_t *second_segment);


#endif // __VECTORS__H__