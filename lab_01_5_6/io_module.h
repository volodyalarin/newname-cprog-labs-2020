#ifndef __IO__MODULE__H__
#define __IO__MODULE__H__

#include "vectors.h"

#define INPUT_ERROR -1
#define POINTER_ERROR -2

#define INPUT_SUCCESS 0

int input_point(point_t *point);
int input_segment(segment_t *segment);

void output_intersect(int is_intersect);

#endif // __IO__MODULE__H__