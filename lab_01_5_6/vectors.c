#include "vectors.h"
#include "math.h"

int points_is_equal(const point_t *first_point, const point_t *second_point)
{
    return 
        fabs(first_point->x - second_point->x) < EPS_FLOAT_EQUALS &&
        fabs(first_point->y - second_point->y) < EPS_FLOAT_EQUALS;
}

segment_t create_segment(const point_t *begin_point, const point_t *end_point)
{
    segment_t segment; 

    segment.begin = *begin_point;
    segment.end = *end_point;

    return segment;
}

vector_t create_vector(const point_t *begin_point, const point_t *end_point)
{
    vector_t vector;

    vector.x = end_point->x - begin_point->x;
    vector.y = end_point->y - begin_point->y;

    return vector;
}

vector_t segment_to_vector(const segment_t *segment)
{
    return create_vector(&segment->begin, &segment->end);
}

double vector_product(const vector_t *vector1, const vector_t *vector2)
{
    return vector1->x * vector2->y - vector1->y * vector2->x;
}

double scalar_product(const vector_t *vector1, const vector_t *vector2)
{
    return vector1->x * vector2->x + vector1->y * vector2->y;
}

int is_belongs_to_segment(const point_t *point, const segment_t *segment)
{
    vector_t vector_s1_s2 = segment_to_vector(segment);
    vector_t vector_s1_p = create_vector(&segment->begin, point);

    vector_t vector_p_s1 = create_vector(point, &segment->begin);
    vector_t vector_p_s2 = create_vector(point, &segment->end);

    return 
        fabs(vector_product(&vector_s1_s2, &vector_s1_p)) < EPS_FLOAT_EQUALS &&
        scalar_product(&vector_p_s1, &vector_p_s2) <= 0;
}

int is_segments_intersect(const segment_t *first_segment, const segment_t *second_segment)
{
    vector_t vector_f1_f2 = segment_to_vector(first_segment);

    vector_t vector_f1_s2 = create_vector(&first_segment->begin, &second_segment->end);
    vector_t vector_f1_s1 = create_vector(&first_segment->begin, &second_segment->begin);

    return 
        vector_product(&vector_f1_f2, &vector_f1_s2) *
        vector_product(&vector_f1_f2, &vector_f1_s1) < 0;
}


