#include <stdio.h>
#include "io_module.h"

double input_resistance()
{
    double resistance = 0;

    printf("Input resistance: ");
    scanf("%lf", &resistance);

    return resistance;
}

void output_resistance(double resistance)
{
    printf("Resistance is %.5lf", resistance);
}
