/*
Три сопротивления R1, R2, R3 соединены параллельно. Найти сопротивление соединения
*/
#include "io_module.h"
#include "resistance.h"

int main()
{
    double resistance1 = input_resistance();
    double resistance2 = input_resistance();
    double resistance3 = input_resistance();

    double resistance = parallel_resistance(resistance1, resistance2, resistance3);

    output_resistance(resistance);
}