#include "resistance.h"

double parallel_resistance(double res1, double res2, double res3)
{
    double resistance = 0;

    if (res1 && res2 && res3)
        resistance = 
            res1 * res2 * res3 /
            (res1 * res2 + res2 * res3 + res1 * res3);

    return resistance;
}

