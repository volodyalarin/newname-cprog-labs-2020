#ifndef __IO__MODULE__H__
#define __IO__MODULE__H__

double input_resistance();
void output_resistance(double resistance);

#endif // __IO__MODULE__H__