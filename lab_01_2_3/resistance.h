#ifndef __RESISTANCE__H__
#define __RESISTANCE__H__

double parallel_resistance(double resistance1, double resistance2, double resistance3);

#endif // __RESISTANCE__H__