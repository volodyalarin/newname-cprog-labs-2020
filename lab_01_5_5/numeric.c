#include "numeric.h"

int input_number(long long *number_pointer)
{
    long long number = 0;
    int err = INPUT_SUCCESS;
    printf("Input number: ");

    if (scanf("%lld", &number) != 1 || number <= 0) 
        err = INPUT_ERROR;
    else
        *number_pointer = number;
    return err;
}


void print_number(const long long number)
{
    printf("Your number is ");

    int digits = get_digits_of_number(number);
    long long tmp = 1;

    for (int i = 0; i < digits - 1; i++)
        tmp *= 10;
    
    while (tmp > 0)
    {
        int i = (number / tmp) % 10;
        printf("%d", i);
        tmp /= 10;
    }

    printf("\n");
}


int get_digits_of_number(const long long number)
{
    int digits = 1;
    long long temp = 10;
    while (temp < number)
    {
        digits++;
        temp *= 10;
    }
    return digits;
}