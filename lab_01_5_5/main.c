/*
Функцию printf(“%d”, i) можно вызывать лишь при i = 0, 1, 2, ..., 9.
Составить программу, печатающую десятичную запись заданного 
натурального числа n > 0.
*/
#include <stdio.h>
#include "numeric.h"

int main()
{
    long long number;
    int err = 0;

    err = input_number(&number) != INPUT_SUCCESS;

    if (!err)
        print_number(number);
    else
        printf("Input error");

    return err;
}