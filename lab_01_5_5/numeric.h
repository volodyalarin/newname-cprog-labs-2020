#ifndef __NUMERIC__H__
#define __NUMERIC__H__

#include <stdio.h>

#define INPUT_ERROR -1
#define INPUT_SUCCESS 0

int input_number(long long *number);
void print_number(const long long number);
int get_digits_of_number(const long long number);

#endif // __NUMERIC__H__