#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>

#define POINTER_ERROR -1
#define DATA_ERROR -2

int process(FILE *file, size_t *pos);

#endif // _UTILS_H_
