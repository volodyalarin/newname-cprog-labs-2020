#include "utils.h"

int process(FILE *file, size_t *pos)
{
    if (!file || !pos)
        return POINTER_ERROR;
    
    *pos = 1;
    long long max;
    int err = fscanf(file, "%lld", &max) != 1 ? DATA_ERROR : 0;
    
    if (!err)
    {
        long long current;
        size_t current_pos = 1;
        while (fscanf(file, "%lld", &current) == 1)
        {
            current_pos++;
            if (current > max)
            {
                *pos = current_pos;
                max = current;
            }
        }
    }
    
     
    return err;
}
