#include <stdio.h>
#include "utils.h"

int main()
{
    size_t result = 0;

    int err = process(stdin, &result);

    if (!err)
        printf("Result: %ld", result);
    else
        printf("Error");
    
    return err;
}
