#ifndef __UNTILS__H__
#define __UNTILS__H__

#include <stdlib.h>
#include <stdbool.h>
#include "matrix.h"

#define POINTER_ERROR -1 

int input_digit(int *digit);

int process_matrix(long long **matrix, size_t *rows, size_t *cols, int digit);

bool contain_digit(long long number, int digit);

#endif // __UNTILS__H__
