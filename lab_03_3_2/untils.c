#include <stdio.h>
#include "untils.h"

int input_digit(int *digit)
{   
    if (!digit)
        return POINTER_ERROR;

    printf("Input digit: ");

    return scanf("%d", digit) != 1 || *digit < 0 || *digit > 9;  
}

int process_matrix(long long **matrix, size_t *rows, size_t *cols, int digit)
{
    if (!matrix || !rows || !cols)
        return POINTER_ERROR;
    
    int err = 0;
    for (size_t j = 0; j < *cols && !err; j++)
    {
        bool contain = false;
        
        for (size_t i = 0; i < *rows && !contain; i++)
            contain = contain_digit(matrix[i][j], digit);
        
        if (contain)
        {
            err = remove_col_matrix(matrix, *rows, *cols, j);
            j--;
            (*cols)--;
        }
    }
    return err;
}

bool contain_digit(long long number, int digit)
{
    if (number < 0) 
        number *= -1;

    bool contain = number == digit;
    while (number && !contain)
    {
        contain = number % 10 == digit;
        number /= 10;
    }

    return contain;
}
