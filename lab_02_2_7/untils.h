#ifndef __UNTILS__H__
#define __UNTILS__H__

#define INPUT_ERROR -1
#define POINTER_ERROR -2

#define INPUT_SUCCESS 0

#define MAX_ARRAY_SIZE 20000

int input_array(long long *array, int *length);
int output_array(long long *array, int length);

int selection_sort(long long *array, const int length);

#endif // __UNTILS__H__
