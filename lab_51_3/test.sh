TARGET=main.out
TARGET_TEST=test.out

make test_file

TESTS=$(find ./tests -name 'in_*.bin')

./$TARGET_TEST c 100 ./temp.out;\

for test in $TESTS; \
    do \
    rm -rf temp.out; \
    cp $test ./temp.out; \
    echo IN DATA:;\
    ./$TARGET_TEST p ./temp.out;\
    ./$TARGET_TEST s ./temp.out;\
    echo RESULT DATA:;\
    ./$TARGET_TEST p ./temp.out;\
    echo;\
    cmp ./temp.out ${test//in_/out_} ; \
    if [ $? != 0 ]
    then
        echo TTEST ERROR
        echo CORRECT DATA:;\
        ./$TARGET_TEST p ${test//in_/out_};\
    fi
done

rm -rf temp.out

make test_coverage

