#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>

#define POINTER_ERROR -1
#define DATA_ERROR -2
#define LAUNCH_ERROR -3
#define IO_ERROR -4
#define SEEK_ERROR -5

#define CREATE_FILE_MODE 1 
#define OUTPUT_FILE_MODE 2 
#define SORT_FILE_MODE   3

#define MAX_RANDOM_INT 100
#define MIN_RANDOM_INT -100

int create_random_file(FILE *file, size_t count);
int output_file(FILE *file);
int sort_file(FILE *file);

int file_size(FILE *file, int *size);
int get_number_by_pos(FILE *file, size_t pos, int *number);
int put_number_by_pos(FILE *file, size_t pos, int number);

int get_mode(int argc, char **argv, int *mode);


#endif // _UTILS_H_
