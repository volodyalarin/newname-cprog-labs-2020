#include <time.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int create_random_file(FILE *file, size_t count)
{
    int err = 0;

    if (!file)
        err = IO_ERROR;

    srand(time(0));

    for (size_t i = 0; i < count && !err; i++)
    {
        int random_number = rand() % (MAX_RANDOM_INT - MIN_RANDOM_INT + 1) + MIN_RANDOM_INT;   
        err = fwrite(&random_number, sizeof(int), 1, file) != 1;
    }

    return err;
}

int output_file(FILE *file)
{
    int err = 0;
    int size;
    int num;

    if (!file)
        err = IO_ERROR;
    
    if (!err)
        err = file_size(file, &size);

    if (!err)
        err = size <= 0 || size % sizeof(int);

    if (!err)
    {
        for (size_t i = 0; i < (size / sizeof(int)) && !err; i++)
        {
            if (fread(&num, sizeof(int), 1, file) == 1)
                printf("%d ", num);
            else
                err = 1;
        }
        printf("\n");
    }

    return err;
}

int file_size(FILE *file, int *size)
{
    int err = 0;
    if (fseek(file, 0, SEEK_END))
        err = SEEK_ERROR;
    if (!err)
        *size = ftell(file);
    if (!err && *size < 0)
        return SEEK_ERROR;
    
    return err || fseek(file, 0, SEEK_SET);
}


int sort_file(FILE *file)
{
    int err = 0;
    int size;

    if (!file)
        err = IO_ERROR;

    if (!err)
        err = file_size(file, &size);
    
    if (!err)
        err = size <= 0 || size % sizeof(int);        

    if (!err)
    {
        size /= sizeof(int);

        int i, key, j;  
        for (i = 1; i < size; i++) 
        {  
            int current;
            j = i;  
            err = get_number_by_pos(file, i, &key);  
            if (!err)
            {
                err = get_number_by_pos(file, j - 1, &current); 
            }
            
            while (!err && j > 0 && current > key) 
            {  
                err = put_number_by_pos(file, j, current);
                j--;
                if (j && !err)
                    err = get_number_by_pos(file, j - 1, &current); 
            } 

            if (!err)
                put_number_by_pos(file, j, key);
        }  
    }

    return err;
}

int get_number_by_pos(FILE *file, size_t pos, int *number)
{
    int err = fseek(file, pos * sizeof(int), SEEK_SET) < 0;
    if (!err)
        err = fread(number, sizeof(int), 1, file) != 1;
    return err;
}

int put_number_by_pos(FILE *file, size_t pos, int number)
{
    int err = fseek(file, pos * sizeof(int), SEEK_SET) < 0;
    if (!err)
        err = fwrite(&number, sizeof(int), 1, file) != 1;
    return err;
}

int get_mode(int argc, char **argv, int *mode)
{
    if (!argv || !mode)
        return POINTER_ERROR;
    
    int err = 0;
    *mode = 0;

    if (argc != 3 && argc != 4)
        err = LAUNCH_ERROR;

    if (!err)
    {
        if (!strcmp(argv[1], "c"))
            *mode = CREATE_FILE_MODE;
        if (!strcmp(argv[1], "p"))
            *mode = OUTPUT_FILE_MODE;
        if (!strcmp(argv[1], "s"))
            *mode = SORT_FILE_MODE;
        if (!(*mode))    
            err = LAUNCH_ERROR; 
    }

    return err;
}
