/*
Для тестирования написан ./test.sh

Выбранные целочисленный тип - int
Алгоритм сортировки - вставками
«Направление» упорядочивания - по-возрастанию.
*/

#include <stdio.h>
#include <stdlib.h>
#include "utils.h"

int main(int argc, char **argv)
{
    int mode = 0;

    int err = get_mode(argc, argv, &mode);

    size_t count = 0;
    if (!err)
    {
        FILE *file = 0;
        switch (mode)
        {
            case CREATE_FILE_MODE:
                err = sscanf(argv[2], "%lu", &count) != 1;
                file = fopen(argv[3], "wb");
                if (!err)
                    err = create_random_file(file, count);
                break;
            case OUTPUT_FILE_MODE:
                file = fopen(argv[2], "rb");
                err = output_file(file);
                break;
            case SORT_FILE_MODE:
                file = fopen(argv[2], "rb+");
                err = sort_file(file);
                break;
        }
        if (file)
            fclose(file);        
    }

    if (err)
        printf("Error \n");
    
    return err;
}
