/*
Треугольник задан координатами своих вершин. Найти периметр треугольника.
*/
#include <stdio.h>
#include <math.h>

#include "coords.h"
#include "io_module.h"

int main()
{
    point_t point1, point2, point3;

    input_point(&point1); 
    input_point(&point2); 
    input_point(&point3); 
    
    double first_side = vector_lenght(&point1, &point2);
    double second_side = vector_lenght(&point2, &point3);
    double third_side = vector_lenght(&point1, &point3);

    double perimeter = count_perimeter(first_side, second_side, third_side);

    output_perimeter(perimeter);

    return 0;    
}