#ifndef __COORDS__H__
#define __COORDS__H__

typedef struct
{
    double x;
    double y;
} point_t;


double vector_lenght(const point_t *start_point, const point_t *end_point);
double count_perimeter(double side1, double side2, double side3);

#endif // __COORDS___H__