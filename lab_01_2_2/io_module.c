#include <stdio.h>

#include "io_module.h"
#include "coords.h"

int input_point(point_t *point_pointer)
{
    int result = INPUT_SUCCESS;

    if (!point_pointer) 
        result = POINTER_ERROR;
    else
    {
        printf("Input point cords: ");
        if (scanf("%lf %lf", &point_pointer->x, &point_pointer->y) != 2)
            result = INPUT_ERROR;
    }
       
    return result;
}

void output_perimeter(double perimeter)
{
    printf("Perimeter is %.5lf", perimeter);
}
