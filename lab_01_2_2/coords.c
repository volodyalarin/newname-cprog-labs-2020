#include "coords.h"
#include <math.h>

double vector_lenght(const point_t *start_point, const point_t *end_point)
{
    return sqrt(pow((start_point->x - end_point->x), 2) 
        + pow((start_point->y - end_point->y), 2));
}

double count_perimeter(double side1, double side2, double side3)
{
    return side1 + side2 + side3; 
}