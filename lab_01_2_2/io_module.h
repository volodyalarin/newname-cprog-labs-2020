#ifndef __IO__MODULE__H__
#define __IO__MODULE__H__

#include "coords.h"

#define INPUT_SUCCESS 0
#define INPUT_ERROR 1
#define POINTER_ERROR 2


int input_point(point_t *point_pointer);
void output_perimeter(double peremeter);

#endif // __IO__MODULE__H__