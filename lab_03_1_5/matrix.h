#ifndef __MATRIX__H__
#define __MATRIX__H__

#define POINTER_ERROR -1

#define MAX_MATRIX_ROWS 10 
#define MAX_MATRIX_COLS 10
 
#include <stdlib.h>


void transform(long long **matrix, long long buffer[][MAX_MATRIX_COLS]);

int input_matrix_size(size_t *rows, size_t *cols);

int input_array(long long *array, size_t length);
int input_matrix(long long **matrix, size_t rows, size_t cols);

int output_matrix(long long **matrix, size_t rows, size_t cols);
int output_array(const long long *array, size_t length);

#endif // __MATRIX__H__
