#include <stdio.h>
#include "untils.h"

int process_matrix(long long **matrix, size_t rows, size_t cols)
{
    if (!matrix)
        return POINTER_ERROR;

    int err = 0;
    size_t length = 0;

    long long elements[MAX_MATRIX_COLS * MAX_MATRIX_ROWS] = { 0 };

    err = find_elements(matrix, rows, cols, elements, &length, is_filtered);

    if (!err)
        err = !length;

    for (size_t i = 0; i < 3 && !err; i++)
        err = array_rotate_left(elements, length);

    if (!err)
        err = restore_elements(matrix, rows, cols, elements, length, is_filtered);
    
    return err;
}

int find_elements(long long **matrix, size_t rows, size_t cols, long long *buffer, size_t *length, bool filter(long long n))
{
    if (!matrix || !buffer || !filter)
        return POINTER_ERROR;

    for (size_t i = 0; i < rows; i++)
        for (size_t j = 0; j < cols; j++)
            if (filter(matrix[i][j]))
            {
                buffer[*length] = matrix[i][j];
                (*length)++;
            }

    return 0;
}

int restore_elements(long long **matrix, size_t rows, size_t cols, long long *buffer, size_t length, bool filter(long long n))
{
    if (!matrix || !buffer || !filter)
        return POINTER_ERROR;

    size_t n = 0;
    for (size_t i = 0; i < rows && n < length; i++)
        for (size_t j = 0; j < cols && n < length; j++)
            if (is_filtered(matrix[i][j]))
            {
                matrix[i][j] = buffer[n];
                n++;
            }

    return n != length;
}

bool is_filtered(long long number)
{
    int sum = 0;
    if (number < 0)
        number *= -1;
    
    while (number > 0 && sum <= 10)
    {
        sum += number % 10;
        number /= 10;
    }

    return sum > 10;
}

int array_rotate_left(long long *array, size_t size)
{
    if (!array)
        return POINTER_ERROR;
    if (!size)
        return 0;

    long long temp = array[0];

    for (size_t i = 0; i < size - 1; i++)
        array[i] = array[i + 1];

    array[size - 1] = temp;

    return 0;
}
