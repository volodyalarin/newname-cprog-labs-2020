#ifndef __UNTILS__H__
#define __UNTILS__H__

#include <stdlib.h>
#include <stdbool.h>
#include "matrix.h"

#define POINTER_ERROR -1 


int process_matrix(long long **matrix, size_t rows, size_t cols);

bool is_filtered(long long number);

int array_rotate_left(long long *array, size_t size);


int find_elements(long long **matrix, size_t rows, size_t cols, long long *buffer, size_t *length, bool filter(long long n));
int restore_elements(long long **matrix, size_t rows, size_t cols, long long *buffer, size_t length, bool filter(long long n));


#endif // __UNTILS__H__
