#ifndef _UNTILS_H_
#define _UNTILS_H_

#include <stdlib.h>
#include <stdbool.h>

#define POINTER_ERROR -1 

#define MAX_STRING_LENGHT 256
#define MAX_WORD_LENGHT 16
#define MAX_WORD_COUNT 128

void transform(char **matrix, char *buffer, size_t rows, size_t cols);

int input_string(char *str, size_t n);

int split_words(char **words, size_t *count, const char *line);


bool is_determintator(char ch);

int remove_word(char *word, char **words, size_t *count);

int remove_element(char **words, size_t pos, size_t count);

int unique_letters(char *word);
int remove_char(char *word, size_t pos);

int store_words(char **words, size_t count, char *string);


#endif // _UNTILS_H_
