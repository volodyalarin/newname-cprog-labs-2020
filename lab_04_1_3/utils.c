#include <stdio.h>
#include <string.h>
#include "utils.h"

int input_string(char *line, size_t line_size)
{
    if (!line)
        return POINTER_ERROR;
    
    char ch;
    size_t i = 0;
    while ((ch = getchar()) != '\n' && ch != -1)
    {
        if (i < line_size - 1)
            line[i] = ch;
        i++;
    }
    
    if (i < line_size)
    {
        line[i] = '\0';
    }
    
    return !i || i >= line_size;
}

void transform(char **matrix, char *buffer, size_t rows, size_t cols)
{
    for (size_t i = 0; i < rows; i++)
        matrix[i] = buffer + i * cols;
}


int split_words(char **words, size_t *count, const char *line)
{
    if (!words || !count || !line)
        return POINTER_ERROR;
    
    int err = 0;
    size_t word_size = 0;
    *count = 0;

    while (*line && !err)
    {
        if (word_size)
        {
            // complete word
            words[*count][word_size] = '\0';
            (*count)++;
            err = *count > MAX_WORD_COUNT;          
        }
        word_size = 0;

        while (is_determintator(*line) && *line && !err)
        {
            // skip determinators
            line++;
        }

        while (!is_determintator(*line) && *line && !err)
        {
            // add char to word
            words[*count][word_size] = *line;
            word_size++;
            err = word_size > MAX_WORD_LENGHT;
            line++;
        }
    }

    if (!err && word_size)
    {
        // complete last word
        words[*count][word_size] = '\0';
        (*count)++;
        err = *count > MAX_WORD_COUNT;
    }
    
    return err;
}

bool is_determintator(char ch)
{
    char determinators[] = { ' ', ',', ';', ':', '-', '.', '!', '?' };

    bool result = false;

    for (size_t i = 0; i < sizeof(determinators) && !result; i++)
        result = ch == determinators[i];
    
    return result;
}

int remove_word(char *word, char **words, size_t *count)
{
    int err = false;
    for (size_t i = 0; i < *count; i++)
    {
        if (!strncmp(words[i], word, MAX_WORD_LENGHT))
        {
            err = remove_element(words, i, *count);
            (*count)--;
            i--;
        }
    }

    return err;    
}

int remove_element(char **words, size_t pos, size_t count)
{
    if (!words)
        return POINTER_ERROR;

    for (size_t i = pos + 1; i < count; i++)
    {
        words[i - 1] = words[i]; 
    }

    return 0;
}


int store_words(char **words, size_t count, char *string)
{  
    if (!words || !string)
        return POINTER_ERROR;
    
    string[0] = '\0';
    for (size_t i = count; i; i--)
    {
        unique_letters(words[i - 1]);
        strncat(string, words[i - 1], MAX_STRING_LENGHT);
        if (i > 1)
            strncat(string, " ", MAX_STRING_LENGHT);
    }
    return 0;
}

int unique_letters(char *word)
{
    int err = 0;
    for (char *pi = word; *pi && !err; pi++)
        for (char *pj = pi + 1; *pj && !err; pj++)
            if (*pi == *pj)
            {
                err = remove_char(word, pj - word);
                pj--;
            }    
    
    return err;
}

int remove_char(char *word, size_t pos)
{
    if (!word)
        return POINTER_ERROR;
    if (pos >= strlen(word))
        return -1;
    
    for (char *i = word + pos; *i; i++)
        *i = *(i + 1);
    
    return 0;
}