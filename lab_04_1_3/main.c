#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"


int main()
{
    char string[MAX_STRING_LENGHT + 1] = { 0 };
    
    char words_buffer[MAX_WORD_COUNT][MAX_WORD_LENGHT + 1] = { 0 };
    char *words[MAX_WORD_COUNT] = { 0 };
    transform(words, *words_buffer, MAX_WORD_COUNT, MAX_WORD_LENGHT + 1);

    size_t words_count = 0;
    int error = 0;

    error = input_string(string, sizeof(string));
    if (!error)
        error = split_words(words, &words_count, string);
        
    if (!error)
        error = !words_count;
    
    if (!error)
        remove_word(words[words_count - 1], words, &words_count);
    
    if (!error)
        error = !words_count;
    
    if (!error)
        store_words(words, words_count, string);

    if (!error)
        printf("Result: %s\n", string);
    else
        printf("Error\n");
    
    return error;
}
