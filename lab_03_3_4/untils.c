#include <stdio.h>
#include "untils.h"

int process_matrix(long long **matrix, size_t size)
{
    if (!matrix)
        return POINTER_ERROR;

    for (size_t i = 0; i < size / 2; i++)
    {
        for (size_t j = i; j < size - i; j++)
            swap(matrix[j] + i, matrix[j] + size - 1 - i, sizeof(**matrix));
    }   
    
    return 0;
}