#ifndef __UNTILS__H__
#define __UNTILS__H__

#include <stdlib.h>
#include <stdbool.h>
#include "matrix.h"

#define POINTER_ERROR -1 

int process_matrix(long long **matrix, size_t size);


#endif // __UNTILS__H__
