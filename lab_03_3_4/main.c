#include "matrix.h"
#include "untils.h"

#include "stdlib.h"
#include "stdio.h"

int main()
{
    long long buffer [MAX_MATRIX_ROWS][MAX_MATRIX_COLS] = { { 0 } };
    long long *matrix[MAX_MATRIX_ROWS];
    transform(matrix, buffer);
    
    int error = 0;
    
    size_t rows, cols = 0;

    error = input_matrix_size(&rows, &cols);

    if (!error)
        error = rows != cols;

    if (!error)
        error = input_matrix(matrix, rows, cols);

    if (!error)
        error = process_matrix(matrix, rows);

    if (!error)
        error = output_matrix(matrix, rows, cols);

    if (error)
        printf("Error \n");
    
    return error;
}
