#include <stdio.h>
#include <math.h>

#include "array.h"

int input_array(int *array, int *length)
{
    if (!array || !length)
        return POINTER_ERROR;
    
    int err = INPUT_SUCCESS;

    printf("Input length of array: ");
    if (scanf("%d", length) != 1 || *length <= 0 || *length > MAX_ARRAY_SIZE)
        err = INPUT_ERROR;
    if (!err)
        printf("Input array elements: ");
    
    for (int i = 0; i < (*length) && !err; i++)
    {
        if (scanf("%d", array + i) != 1)
            err = INPUT_ERROR; 
    }
    
    return err;
}

int filter_positive_array(int *array, int *length, int *filtered_array, int *filtered_length)
{ 
    if (!array || !length || !filtered_array || !filtered_length)
        return POINTER_ERROR;

    *filtered_length = 0;
    for (int i = 0; i < *length; i++)
        if (array[i] > 0)
        {
            filtered_array[*filtered_length] = array[i];
            (*filtered_length)++;
        }
            
    return 0;
}
double geometric_mean(int *array, int *length)
{
    if (!array || !length)
        return POINTER_ERROR;
    
    int product = 1;

    for (int i = 0; i < *length; i++)
        product *= array[i];
    
    return pow(product, 1.0 / (double)(*length));
}

void print_result(double result)
{
    printf("Result is %lf", result);
}
