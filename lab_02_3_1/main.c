#include <stdio.h>
#include "array.h"

int main(void)
{
    int err = 0;

    int array[MAX_ARRAY_SIZE] = { 0 };
    int length = 0;

    int filtered[MAX_ARRAY_SIZE] = { 0 };
    int filtered_length = 0;

    if (!err)
        err = input_array(array, &length);

    if (!err)
        err = filter_positive_array(array, &length, filtered, &filtered_length);

    if (!err)
        err = filtered_length == 0;

    double res = 0;
    if (!err)
        res = geometric_mean(filtered, &filtered_length);

    if (!err)
        print_result(res);
    else
        printf("error");
    return err;
}
