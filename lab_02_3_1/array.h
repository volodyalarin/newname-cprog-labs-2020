#ifndef __ARRAY__H__
#define __ARRAY__H__

#define INPUT_ERROR -1
#define POINTER_ERROR -2

#define INPUT_SUCCESS 0

#define MAX_ARRAY_SIZE 10


int input_array(int *array, int *length);

int filter_positive_array(int *array, int *length, int *filtered_array, int *filtered_length);
double geometric_mean(int *array, int *length);
void print_result(double result);

#endif // __ARRAY__H__
