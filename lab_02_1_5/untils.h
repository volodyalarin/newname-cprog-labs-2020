#ifndef __UNTILS__H__
#define __UNTILS__H__

#define INPUT_ERROR -1
#define POINTER_ERROR -2

#define INPUT_SUCCESS 0

#define MAX_ARRAY_SIZE 10


int input_array(long long *array, long long **end);

int process_array(long long *array, long long *end, long long *result);

void print_result(long long result);


#endif // __UNTILS__H__
