#include <stdio.h>
#include "untils.h"

int input_array(long long *array, long long **end)
{
    if (!array || !end)
        return POINTER_ERROR;
    
    int err = INPUT_SUCCESS;

    int length = 0;

    printf("Input length of array: ");

    if (scanf("%d", &length) != 1 || length <= 0 || length > MAX_ARRAY_SIZE)
        err = INPUT_ERROR;
    
    if (!err)
        printf("Input array elements: ");
    
    *end = array + length;
    
    for (long long *pi = array; pi != *end && !err; pi++)
    {
        if (scanf("%lld", pi) != 1)
            err = INPUT_ERROR; 
    }
    
    return err;
}

int process_array(long long *array, long long *end, long long *result)
{
    if (!array || !end || !result)
        return POINTER_ERROR;
    
    *result = 0;

    long long *pi = array;
    long long *pj = end - 1;

    while (pi < end && pj > array)
    {        
        while (*pi >= 0 && pi < end) 
            pi++;
        while (*pj <= 0 && pj > array) 
            pj--;
        
        if (pi < end && pj > array)
            *result += (*pi) * (*pj);
        
        pi++;
        pj--;
    }
    
    return *result == 0;
}

void print_result(long long result)
{
    printf("Result is %lld\n", result);
}