#include <stdio.h>
#include "untils.h"

int main(void)
{
    int err = 0;

    long long array[MAX_ARRAY_SIZE] = { 0 };
    long long *end = 0;
    long long result = 0;

    if (!err)
        err = input_array(array, &end);
    
    if (!err)
        err = process_array(array, end, &result);
    
    if (!err)
        print_result(result);
    else
        printf("error\n");
    
    return err;
}
