#include "utils.h"

int process(FILE *file, size_t *count)
{
    if (!file || !count)
        return POINTER_ERROR;
    
    *count = 0;

    double max, min, avarage;

    int err = get_max_min_elements(file, &max, &min);

    if (!err)
        avarage = (max + min) * 0.5;

    if (!err)
        err = fseek(file, 0, SEEK_SET) < 0;

    if (!err)
        err = count_elements(file, count, avarage);

    return err;
}

int get_max_min_elements(FILE *file, double *max, double *min)
{
    if (!file || !max || !min)
        return POINTER_ERROR;

    double current;

    int err = fscanf(file, "%lf", &current) != 1 ? DATA_ERROR : 0;
    
    *min = *max = current;

    while (!err && fscanf(file, "%lf", &current) == 1)
    {
        if (current > *max)
            *max = current;
        if (current < *min)
            *min = current;
    }

    return err;
}

int count_elements(FILE *file, size_t *count, double avarage)
{
    if (!file || !count)
        return POINTER_ERROR;
    
    double current;

    while (fscanf(file, "%lf", &current) == 1)
        if (current > avarage)
            (*count)++;
    
    return 0;
}
