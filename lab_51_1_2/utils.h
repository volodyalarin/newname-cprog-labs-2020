#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdio.h>

#define POINTER_ERROR -1
#define DATA_ERROR -2
#define LAUNCH_ERROR -3
#define IO_ERROR -4

int process(FILE *file, size_t *count);
int get_max_min_elements(FILE *file, double *max, double *min);
int count_elements(FILE *file, size_t *count, double avarage);

#endif // _UTILS_H_
