#include <stdio.h>
#include "utils.h"

int main(int argc, char **argv)
{
    int err = 0;
    size_t result = 0;
    FILE *file;

    if (argc != 2)
        err = LAUNCH_ERROR;

    if (!err)    
        file = fopen(argv[1], "r");

    if (!file)
        err = IO_ERROR;
    
    if (!err)
        err = process(file, &result);

    if (!err)
        printf("Result: %ld \n", result);
    else
        printf("Error \n");
    
    return err;
}
