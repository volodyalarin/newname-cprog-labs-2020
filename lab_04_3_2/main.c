#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"

int main()
{
    char first_string[MAX_STRING_LENGHT + 1] = { 0 };
    char second_string[MAX_STRING_LENGHT + 1] = { 0 };
    
    char words_buffer[MAX_WORD_COUNT * 2][MAX_WORD_LENGHT + 1] = { 0 };

    char *words[MAX_WORD_COUNT * 2] = { 0 };

    size_t words_count = 0, words_count_2 = 0;


    transform(words, *words_buffer, MAX_WORD_COUNT * 2, MAX_WORD_LENGHT + 1);


    int error = 0;


    error = input_string(first_string, sizeof(first_string));
    if (!error)
        error = input_string(second_string, sizeof(second_string));   

    if (!error)
        error = split_words(words, &words_count, first_string);
        
    if (!error)
        error = split_words(words + words_count, &words_count_2, second_string);
    
    if (!error)
        words_count += words_count_2;
    
    if (!error)
    {
        error = !words_count;
    }
    

    if (!error)
        unique_words(words, &words_count);
            
    if (!error)
        print_words(words, words_count);
    else
        printf("Error\n");
    
    return error;
}
