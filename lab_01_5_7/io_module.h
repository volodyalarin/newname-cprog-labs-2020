#ifndef __IO__MODULE__H__
#define __IO__MODULE__H__

#define INPUT_ERROR -1;
#define POINTER_ERROR -2;

#define INPUT_SUCCESS 0

int input_value(double *value);
int input_eps(double *eps);


void output_absolute_value(double value);
void output_relative_value(double value);

void output_absolute_difference(double difference);
void output_relative_difference(double difference);

#endif // __IO__MODULE__H__