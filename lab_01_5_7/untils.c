#include <math.h>
#include "untils.h"

double f(double x)
{
    return 1 / sqrt(1 - x * x);
}

double s(double x, double last, unsigned int n)
{
    double res = last;
    if (!n)
        res = 1;
    else
    {
        res *= x * x;
        res *= 2 * n - 1;
        res /= 2 * n;
    }
    return res;    
}


double find_with_precision(double x, double (*func)(double x, double last, unsigned int n), double eps)
{
    unsigned int n = 0;
    double current_value = (*func)(x, 0, n);
    double sum_func = current_value;

    for (n++; current_value > eps; n++)
    {
        current_value = (*func)(x, current_value, n);
        sum_func += current_value;
    }

    return sum_func;
}

double get_absolute_difference(double x, double y)
{
    return fabs(x - y);
}

double get_relative_difference(double x, double y)
{
    return fabs(1 - y / x);
}

