/*
Определить пересекаются ли два отрезка.
Вычислить с точность eps:
− приближенное значение функции s(x);
− точное значение функции f(x);
− абсолютную |f(x) -s(x)| и относительную |(f(x) - s(x)) / f(x)| ошибки приближенного значения.

f(x) = 1 / sqrt(1-x^2)
s(x)_n = (2n - 1)! * x^n / (2n)!
s(x)_1 = 1 


*/
#include <stdio.h>

#include "io_module.h"
#include "untils.h"

int main()
{
    int res = 0;
    double x, eps;

    res = input_value(&x);
    if (res == INPUT_SUCCESS)
        res = input_eps(&eps);
    
    double absolute_value = 0, relative_value = 0; 
    double absolute_difference = 0, relative_difference = 0; 
    if (res == INPUT_SUCCESS)
    {
        absolute_value = f(x);
        relative_value = find_with_precision(x, &s, eps);

        absolute_difference = get_absolute_difference(absolute_value, relative_value);
        relative_difference = get_relative_difference(absolute_value, relative_value);

        output_absolute_value(absolute_value);
        output_relative_value(relative_value);

        output_absolute_difference(absolute_difference);
        output_relative_difference(relative_difference);
    }
    else
    {
        printf("Error \n");
    }
    

    return res;
}
