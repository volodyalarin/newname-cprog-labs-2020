#ifndef __UNTILS__H__
#define __UNTILS__H__

double f(double x);
double s(double x, double last, unsigned int n);


double find_with_precision(double x, double (*func)(double x, double last, unsigned int n), double eps);

double get_absolute_difference(double x, double y);
double get_relative_difference(double x, double y);

#endif // __UNTILS__H__