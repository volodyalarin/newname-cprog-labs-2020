#include <stdio.h>
#include "io_module.h" 

int input_value(double *value)
{
    int err = INPUT_SUCCESS;
    if (!value)
    {
        err = POINTER_ERROR;
    }
    else
    {
        printf("Input value of parameter (x): ");

        if (scanf("%lf", value) != 1 || *value >= 1 || *value <= -1)
            err = INPUT_ERROR;
    }    
    return err;    
}

int input_eps(double *eps)
{
    int err = INPUT_SUCCESS;
    if (!eps)
    {
        err = POINTER_ERROR;
    }
    else
    {
        printf("Input precision: ");

        if (scanf("%lf", eps) != 1 || *eps <= 0 || *eps >= 1)
            err = INPUT_ERROR;
    }    
    return err;    
}


void output_absolute_value(double value)
{
    printf("Absolute value is %lf \n", value);
}

void output_relative_value(double value)
{
    printf("Relative value is %lf \n", value);
}

void output_absolute_difference(double difference)
{
    printf("Absolute difference is %lf \n", difference);
}

void output_relative_difference(double difference)
{
    printf("Relative difference is %lf \n", difference);
}

