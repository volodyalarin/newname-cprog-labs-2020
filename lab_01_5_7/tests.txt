Input:
1
Output:
error

Input:
2e-1
-2
Output:
error

Input:
2e-1
a
Output:
error

Input:
2e-1
1e-4
Output:
1.020621
1.020620

0.000001
0.000001

