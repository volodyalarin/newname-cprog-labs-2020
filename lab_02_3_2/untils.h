#ifndef __UNTILS__H__
#define __UNTILS__H__

#define INPUT_ERROR -1
#define POINTER_ERROR -2

#define INPUT_SUCCESS 0

#define MAX_ARRAY_SIZE 10


int input_array(long long *array, int *length);
int output_array(long long *array, int length);


int filter_array(long long *array, int *length, long long *filtered_array, int *filtered_length, int filter(long long x));
int is_armstrong_number(long long number);

int get_digits_of_number(const long long number);
long long my_pow(const long long number, const int power);

#endif // __UNTILS__H__
