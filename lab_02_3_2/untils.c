#include <stdio.h>
#include "untils.h"

int input_array(long long *array, int *length)
{
    if (!array || !length)
        return POINTER_ERROR;
    
    int err = INPUT_SUCCESS;

    printf("Input length of array: ");
    if (scanf("%d", length) != 1 || *length <= 0 || *length > MAX_ARRAY_SIZE)
        err = INPUT_ERROR;
    if (!err)
        printf("Input array elements: ");
    
    for (int i = 0; i < (*length) && !err; i++)
    {
        if (scanf("%lld", array + i) != 1)
            err = INPUT_ERROR; 
    }
    
    return err;
}

int output_array(long long *array, int length)
{
    if (!array)
        return POINTER_ERROR;
    
    printf("Your array is \n");
    for (int i = 0; i < length; i++)
        printf("%lld ", array[i]);
    printf("\n");

    return 0;
}

int filter_array(long long *array, int *length, long long *filtered_array, int *filtered_length, int filter(long long x))
{ 
    if (!array || !length || !filtered_array || !filtered_length)
        return POINTER_ERROR;

    *filtered_length = 0;
    for (int i = 0; i < *length; i++)
        if (filter(array[i]))
        {
            filtered_array[*filtered_length] = array[i];
            (*filtered_length)++;
        }
            
    return 0;
}

int is_armstrong_number(long long number)
{
    int result = number >= 0;

    int digits = get_digits_of_number(number);

    long long temp_result = 0;
    long long temp_number = number;

    if (result)
    {
        while (temp_number > 0 && temp_result <= number)
        {
            temp_result += my_pow(temp_number % 10, digits);
            temp_number /= 10;
        }
        result = temp_result == number;
    }

    return result;
}

long long my_pow(const long long number, const int power)
{
    long long result = 1;
    for (int i = 0; i < power; i++)
        result *= number;
    return result;
}

int get_digits_of_number(const long long number)
{
    int digits = 1;
    long long temp = 10;
    while (temp < number)
    {
        digits++;
        temp *= 10;
    }
    return digits;
}
