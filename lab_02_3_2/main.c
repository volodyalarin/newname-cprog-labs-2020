#include <stdio.h>
#include "untils.h"

int main(void)
{
    int err = 0;

    long long array[MAX_ARRAY_SIZE] = { 0 };
    int length = 0;

    long long filtered[MAX_ARRAY_SIZE] = { 0 };
    int filtered_length = 0;

    if (!err)
        err = input_array(array, &length);

    if (!err)
        err = filter_array(array, &length, filtered, &filtered_length, is_armstrong_number);

    if (!err)
        err = filtered_length == 0;

    if (!err)
        output_array(filtered, filtered_length);
    else
        printf("error \n");
    return err;
}
