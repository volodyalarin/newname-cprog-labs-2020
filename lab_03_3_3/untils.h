#ifndef __UNTILS__H__
#define __UNTILS__H__

#include <stdlib.h>
#include <stdbool.h>
#include "matrix.h"

#define POINTER_ERROR -1 

int sort_rows_matrix(long long **matrix, size_t rows, size_t cols,
bool (*cmp)(long long *a, long long *b, size_t cols));
long long min_element(long long *array, size_t cols);
bool is_greater_row(long long *row_a, long long *row_b, size_t cols);


#endif // __UNTILS__H__
