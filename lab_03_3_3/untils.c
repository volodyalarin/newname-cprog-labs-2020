#include <stdio.h>
#include "untils.h"

int sort_rows_matrix(long long **matrix, size_t rows, size_t cols,
bool (*cmp)(long long *a, long long *b, size_t cols))
{
    if (!matrix)
        return POINTER_ERROR;
    
    for (size_t i = 1; i < rows; i++)
    {  
        size_t j = i; 
        while (j > 0 && !cmp(matrix[j - 1], matrix[j], cols))
        {
            swap(matrix + j, matrix + j - 1, sizeof(matrix));
            j--;
        }
    }
    return 0;
}

long long min_element(long long *array, size_t cols)
{
    long long min = array[0];
    for (size_t i = 1; i < cols; i++)
        if (min > array[i])
            min = array[i];
    
    return min;    
}
bool is_greater_row(long long *row_a, long long *row_b, size_t cols)
{
    long long a = min_element(row_a, cols);
    long long b = min_element(row_b, cols);
    return a >= b;
}