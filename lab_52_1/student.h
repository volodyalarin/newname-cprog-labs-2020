#ifndef _STUDENT_H_
#define _STUDENT_H_

#include <stdint.h>
#include "structs.h"

int append_student(data_t *data, student_t *student);
int search_student(data_t *data, char *substr);
int sort_students(data_t *data, int cmp(student_t *, student_t *));
int cmp_students(student_t *a, student_t *b);
int remove_students(data_t *data, double avg);
double student_avg(student_t *student);
int calc_avarage_mark(data_t *data, double *avarage);

int get_student_by_pos(data_t *data, size_t pos, student_t *student);
int put_student_by_pos(data_t *data, size_t pos, student_t *student);

#endif // _STUDENT_H_
