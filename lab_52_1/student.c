#include <string.h>

#include "student.h"
#include "utils.h"

/**
 * @brief Добавляет студента
 * 
 * @param data объект данных
 * @param student 
 * @return err статус выполнения
 */
int append_student(data_t *data, student_t *student)
{
    if (!data || !student)
        return POINTER_ERROR;

    int err = put_student_by_pos(data, data->lenght, student);
    if (!err)
        data->lenght++;

    return err;
}

/**
 * @brief Поиск студентов по подстроке фамилии
 * 
 * @param data объект данных
 * @param substr 
 * @return err статус выполнения
 */
int search_student(data_t *data, char *substr)
{
    int err = 0;
    if (!data->lenght)
        err = DATA_ERROR;

    size_t len = strlen(substr);
    size_t new_lenght = 0;
    for (size_t i = 0; !err && i < data->lenght; i++)
    {
        student_t student = { 0 };
        memset(&student, 0, sizeof(student_t));

        err = get_student_by_pos(data, i, &student);

        if (!err && !strncmp(student.surname, substr, len))
        {
            err = put_student_by_pos(data, new_lenght, &student);
            new_lenght++;
        }
    }

    if (!err)
        data->lenght = new_lenght;

    return err;
}
/**
 * @brief Сортирует студентов
 * 
 * @param data объект данных
 * @param cmp функция сравнения студентов
 * @return err статус выполнения
 */
int sort_students(data_t *data, int cmp(student_t *, student_t *))
{
    int err = 0;
    if (!data->lenght)
        err = DATA_ERROR;
    int i, j;
    student_t key = { 0 };
    memset(&key, 0, sizeof(student_t));

    for (i = 1; !err && i < (int)data->lenght; i++)
    {
        student_t current = { 0 };
        memset(&current, 0, sizeof(student_t));

        j = i;
        err = get_student_by_pos(data, i, &key);
        if (!err)
        {
            err = get_student_by_pos(data, j - 1, &current);
        }

        while (!err && j > 0 && cmp(&current, &key) > 0)
        {
            err = put_student_by_pos(data, j, &current);
            j--;
            if (j && !err)
                err = get_student_by_pos(data, j - 1, &current);
        }

        if (!err)
            put_student_by_pos(data, j, &key);
    }

    return err;
}

/**
 * @brief Сравнивает студентов по фамилии и имени
 * 
 * @param a 
 * @param b 
 * @return err статус выполнения
 */
int cmp_students(student_t *a, student_t *b)
{
    int res = strcmp(a->surname, b->surname);
    if (!res)
        res = strcmp(a->name, b->name);
    return res;
}

/**
 * @brief Удаляет студентов с малым средним баллом
 * 
 * @param data объект данных
 * @param avg 
 * @return err статус выполнения
 */
int remove_students(data_t *data, double avg)
{
    int err = 0;
    size_t new_lenght = 0;
    for (size_t i = 0; !err && i < data->lenght; i++)
    {
        student_t student = { 0 };
        memset(&student, 0, sizeof(student_t));

        err = get_student_by_pos(data, i, &student);
        if (!err && student_avg(&student) >= avg)
        {
            err = put_student_by_pos(data, new_lenght, &student);
            new_lenght++;
        }
    }

    if (!err)
        data->lenght = new_lenght;

    return err;
}

/**
 * @brief Подсчитывает средний бал студента
 * 
 * @param student 
 * @return double 
 */
double student_avg(student_t *student)
{
    int sum = 0;
    for (size_t j = 0; j < MARKS_LEN; j++)
        sum += student->marks[j];
    return sum / MARKS_LEN;
}


/**
 * @brief Считает средний балл всех студентов
 * 
 * @param data объект данных
 * @param avarage 
 * @return err статус выполнения
 */
int calc_avarage_mark(data_t *data, double *avarage)
{
    int err = 0;

    double sum = 0;
    for (size_t i = 0; i < data->lenght; i++)
    {
        student_t student = { 0 };
        memset(&student, 0, sizeof(student_t));

        get_student_by_pos(data, i, &student);
        sum += student_avg(&student);
    }
    if (data->lenght)
        *avarage = sum / data->lenght;
    else
        err = DATA_ERROR;

    return err;
}


/**
 * @brief Получение студента по позиции
 * 
 * @param data объект данных
 * @param pos 
 * @param student 
 * @return err статус выполнения
 */
int get_student_by_pos(data_t *data, size_t pos, student_t *student)
{
    if (!data || !student)
        return POINTER_ERROR;

    int err = 0;
    switch (data->mode)
    {
        case TEXT_MODE:
            get_student_by_pos_array(data, pos, student);
            break;
        case BINARY_MODE:
            err = get_student_by_pos_binary(data->file_in, pos, student);
            break;
    }

    return err;
}

/**
 * @brief Сохрание студента по позиции
 * 
 * @param data объект данных
 * @param pos 
 * @param student 
 * @return err статус выполнения
 */
int put_student_by_pos(data_t *data, size_t pos, student_t *student)
{
    if (!data || !student)
        return POINTER_ERROR;

    int err = 0;
    switch (data->mode)
    {
        case TEXT_MODE:
            put_student_by_pos_array(data, pos, student);
            break;
        case BINARY_MODE:
            err = put_student_by_pos_binary(data->file_out, pos, student);
            break;
    }

    return err;
}