#ifndef _STRUCTS_H_
#define _STRUCTS_H_

#include <stdint.h>
#include <stdio.h>


#define POINTER_ERROR -1
#define DATA_ERROR -2
#define LAUNCH_ERROR 53
#define IO_ERROR -4
#define SEEK_ERROR -5

#define APPEND_ACTION 1
#define SEARCH_ACTION 2
#define SORT_ACTION 3
#define FILTER_ACTION 4

#define BINARY_MODE 1
#define TEXT_MODE 2


#define MAX_SURNAME_LEN 25
#define MAX_NAME_LEN 10
#define MARKS_LEN 4
#define MAX_ARRAY_LEN 100

typedef struct
{
    char surname[MAX_SURNAME_LEN + 1];
    char name[MAX_NAME_LEN + 1];
    uint32_t marks[MARKS_LEN];
} student_t;

typedef struct
{
    int mode;

    FILE *file_in;
    FILE *file_out;

    student_t array[MAX_ARRAY_LEN];
    size_t lenght;
} data_t;


#endif // _STRUCTS_H_
