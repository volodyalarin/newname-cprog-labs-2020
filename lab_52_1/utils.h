#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>
#include <stdio.h>
#include "structs.h"

int append_student_action(data_t *data);
int filter_student_action(data_t *data);

int file_size(FILE *file, int *size);

int init_data(data_t *data, int mode, int action, char *filename_in, char *filename_out);
int finish_data(data_t *data);

int get_student_by_pos_binary(FILE *file, size_t pos, student_t *student);
int put_student_by_pos_binary(FILE *file, size_t pos, student_t *student);

int parse_text_file(FILE *file, student_t *student);
int save_text_file(FILE *file, student_t *student);

void get_student_by_pos_array(data_t *data, size_t pos, student_t *student);
void put_student_by_pos_array(data_t *data, size_t pos, student_t *student);

int get_launch_params(int argc, char **argv, int *action, int *mode, char **filename_in, char **filename_out, char **substr);

int input_string(FILE *file, char *str, size_t len);

#endif // _UTILS_H_
