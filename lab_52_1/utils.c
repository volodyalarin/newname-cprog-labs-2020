#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <sys/types.h>
#include <unistd.h>

#include "utils.h"
#include "student.h"

/**
 * @brief Добавляет студента из стандартного потока ввода - вывода
 * 
 * @param data объект данных
 * @param student 
 * @return err статус выполнения
 */
int append_student_action(data_t *data)
{
    if (!data)
        return POINTER_ERROR;
    student_t student = { 0 };
    memset(&student, 0, sizeof(student_t));

    printf("Введите почтрочно фимилию, имя, оценки.\n");
    int err = parse_text_file(stdin, &student);
    if (!err)
        err = append_student(data, &student);
    return err;
}

/**
 * @brief 
 * 
 * @param data объект данных
 * @return err статус выполнения
 */
int filter_student_action(data_t *data)
{
    double avarage = 0;

    int err = calc_avarage_mark(data, &avarage);

    if (!err)
        err = remove_students(data, avarage);

    return err;
}


/**
 * @brief Определяет размер файла
 * 
 * @param file 
 * @param size 
 * @return err статус выполнения
 */
int file_size(FILE *file, int *size)
{
    int err = 0;
    if (fseek(file, 0, SEEK_END))
        err = SEEK_ERROR;
    if (!err)
        *size = ftell(file);
    if (!err && *size < 0)
        return SEEK_ERROR;

    return err || fseek(file, 0, SEEK_SET);
}

/**
 * @brief Получает следующего пользователя из текстового файла
 * 
 * @param file 
 * @param student 
 * @return err статус выполнения
 */
int parse_text_file(FILE *file, student_t *student)
{
    if (!file && !student)
        return POINTER_ERROR;

    int err = 0;
    err = input_string(file, student->surname, MAX_SURNAME_LEN);
    if (!err)
        err = input_string(file, student->name, MAX_NAME_LEN);
    if (!err)
    {
        for (size_t i = 0; !err && i < MARKS_LEN; i++)
            if (!err)
                err = fscanf(file, "%u", student->marks + i) != 1;
        fscanf(file, "\n");
    }
    return err;
}

/**
 * @brief Сохраняет студента в текстовый файл
 * 
 * @param file 
 * @param student 
 * @return err статус выполнения
 */
int save_text_file(FILE *file, student_t *student)
{
    if (!file && !student)
        return POINTER_ERROR;
    int err = 0;

    err = fprintf(file, "%s\n", student->surname) < 1;
    if (!err)
        err = fprintf(file, "%s\n", student->name) < 1;

    for (size_t i = 0; !err && i < (MARKS_LEN - 1); i++)
        err = fprintf(file, "%u ", student->marks[i]) < 1;
    
    if (!err)
        err = fprintf(file, "%u\n", student->marks[MARKS_LEN - 1]) < 1;
    

    return err;
}

/**
 * @brief Подготавливает объект данных
 * 
 * @param data объект данных
 * @param mode 
 * @param action 
 * @param filename_in 
 * @param filename_out 
 * @return err статус выполнения
 */
int init_data(data_t *data, int mode, int action, char *filename_in, char *filename_out)
{
    int err = 0;
    data->mode = mode;

    switch (action)
    {
        case FILTER_ACTION:
        case APPEND_ACTION:
            switch (mode)
            {
                case TEXT_MODE:
                    data->file_out = data->file_in = fopen(filename_in, "r+");
                    break;
                case BINARY_MODE:
                    data->file_out = data->file_in = fopen(filename_in, "rb+");
                    break;
            }
            break;
        case SORT_ACTION:
            switch (mode)
            {
                case TEXT_MODE:
                    data->file_in = fopen(filename_in, "r");
                    data->file_out = stdout;
                    break;
                case BINARY_MODE:
                    data->file_out = data->file_in = fopen(filename_in, "rb+");
                    break;
            }
            break;
        case SEARCH_ACTION:
            switch (mode)
            {
                case TEXT_MODE:
                    data->file_in = fopen(filename_in, "r");
                    data->file_out = fopen(filename_out, "w");
                    break;
                case BINARY_MODE:
                    data->file_in = fopen(filename_in, "rb");
                    data->file_out = fopen(filename_out, "wb");
                    break;
            }
    }
    if (!data->file_in || !data->file_out)
        err = IO_ERROR;
    if (!err)
    {
        int temp_size = 0;
        err = file_size(data->file_in, &temp_size);
        switch (mode)
        {
            case TEXT_MODE:
                data->lenght = 0;
                while (!err && ftell(data->file_in) < (temp_size - 1) && data->lenght < MAX_ARRAY_LEN)
                {
                    err = parse_text_file(data->file_in, data->array + data->lenght);
                    data->lenght++;
                }
                if (!err && ftell(data->file_in) < (temp_size - 1))
                    err = DATA_ERROR;
                break;
            case BINARY_MODE:
                if (!err)
                    err = temp_size < 0 || temp_size % sizeof(student_t);
                if (!err)
                    data->lenght = temp_size / sizeof(student_t);
                break;
        }
    }
    return err;
}

/**
 * @brief Завершает обработку данных
 * 
 * @param data объект данных
 * @return err статус выполнения
 */
int finish_data(data_t *data)
{
    int err = 0;
    size_t file_size = 0;
    switch (data->mode)
    {
        case TEXT_MODE:
            if (data->file_out != stdout)
                err = fseek(data->file_out, 0, SEEK_SET);
            for (size_t i = 0; !err && i < data->lenght; i++)
                err = save_text_file(data->file_out, data->array + i);
            if (!err)
                file_size = ftell(data->file_out);
            break;
        case BINARY_MODE:
            file_size = data->lenght * sizeof(student_t);
            break;
    }

    if (!err && !data->lenght)
        err = DATA_ERROR;

    if (!err && data->file_out != stdout)
        ftruncate(fileno(data->file_in), file_size);
    
    int is_equal = data->file_in == data->file_out;

    if (!err)
    {
        fclose(data->file_in);
        if (!is_equal && data->file_out != stdout)
            fclose(data->file_out);
    }

    return err;
}


/**
 * @brief Получение студента по позиции в бинарном файле
 * 
 * @param file 
 * @param pos 
 * @param student 
 * @return err статус выполнения
 */
int get_student_by_pos_binary(FILE *file, size_t pos, student_t *student)
{
    int err = fseek(file, pos * sizeof(student_t), SEEK_SET) < 0;
    if (!err)
        err = fread(student, sizeof(student_t), 1, file) != 1;
    return err;
}

/**
 * @brief Сохранение студента по позиции в бинарном файле
 * 
 * @param file 
 * @param pos 
 * @param student 
 * @return err статус выполнения
 */
int put_student_by_pos_binary(FILE *file, size_t pos, student_t *student)
{
    int err = fseek(file, pos * sizeof(student_t), SEEK_SET) < 0;
    if (!err)
        err = fwrite(student, sizeof(student_t), 1, file) != 1;
    return err;
}

/**
 * @brief Получение студента по позиции из массива
 * 
 * @param data объект данных
 * @param pos 
 * @param student 
 */
void get_student_by_pos_array(data_t *data, size_t pos, student_t *student)
{
    *student = data->array[pos];
}

/**
 * @brief Сохранение студента по позиции в массиве
 * 
 * @param data объект данных
 * @param pos 
 * @param student 
 */
void put_student_by_pos_array(data_t *data, size_t pos, student_t *student)
{
    data->array[pos] = *student;
}

/**
 * @brief Get the launch params
 * 
 * @param argc 
 * @param argv 
 * @param action 
 * @param mode 
 * @param filename_in 
 * @param filename_out 
 * @param substr 
 * @return err статус выполнения
 */
int get_launch_params(int argc, char **argv, int *action, int *mode, char **filename_in, char **filename_out, char **substr)
{
    *action = *mode = 0;

    int err = 0;

    if (argc == 3 && !strcmp(argv[1], "st"))
    {
        *action = SORT_ACTION;
        *mode = TEXT_MODE;
        *filename_in = argv[2];
    }
    else if (argc == 3 && !strcmp(argv[1], "sb"))
    {
        *action = SORT_ACTION;
        *mode = BINARY_MODE;
        *filename_in = argv[2];
    }
    else if (argc == 3 && !strcmp(argv[1], "at"))
    {
        *action = APPEND_ACTION;
        *mode = TEXT_MODE;
        *filename_in = argv[2];
    }
    else if (argc == 3 && !strcmp(argv[1], "ab"))
    {
        *action = APPEND_ACTION;
        *mode = BINARY_MODE;
        *filename_in = argv[2];
    }
    else if (argc == 3 && !strcmp(argv[1], "dt"))
    {
        *action = FILTER_ACTION;
        *mode = TEXT_MODE;
        *filename_in = argv[2];
    }
    else if (argc == 3 && !strcmp(argv[1], "db"))
    {
        *action = FILTER_ACTION;
        *mode = BINARY_MODE;
        *filename_in = argv[2];
    }
    else if (argc == 5 && !strcmp(argv[1], "ft"))
    {
        *action = SEARCH_ACTION;
        *mode = TEXT_MODE;
        *filename_in = argv[2];
        *filename_out = argv[3];
        *substr = argv[4];
    }
    else if (argc == 5 && !strcmp(argv[1], "fb"))
    {
        *action = SEARCH_ACTION;
        *mode = BINARY_MODE;
        *filename_in = argv[2];
        *filename_out = argv[3];
        *substr = argv[4];
    }
    else
    {
        err = LAUNCH_ERROR;
    }

    return err;
}

/**
 * @brief Ввод строки из файла
 * 
 * @param file 
 * @param str 
 * @param len максимальная длина строки
 * @return err статус выполнения
 */
int input_string(FILE *file, char *str, size_t len)
{
    size_t i;
    char cur = fgetc(file);

    for (i = 0; i < len && cur > 0 && cur != '\n'; i++)
    {
        str[i] = cur;
        cur = fgetc(file);
    }

    str[i] = '\0';

    return cur != '\n' && cur != EOF;
}
