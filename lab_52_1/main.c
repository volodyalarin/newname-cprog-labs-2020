#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "student.h"

int main(int argc, char **argv)
{
    data_t data = { 0 };
    memset(&data, 0, sizeof(data_t));

    int mode = 0, action = 0;
    char *substr = 0, *filename_in = 0, *filename_out = 0;

    int err = 0;
    err = get_launch_params(argc, argv, &action, &mode, &filename_in, &filename_out, &substr);

    if (!err)
        err = init_data(&data, mode, action, filename_in, filename_out);

    if (!err)
    {
        switch (action)
        {
            case SORT_ACTION:
                err = sort_students(&data, cmp_students);             
                break;
            case SEARCH_ACTION:
                err = search_student(&data, substr);
                break;
            case FILTER_ACTION:
                err = filter_student_action(&data);
                break;
            case APPEND_ACTION:
                err = append_student_action(&data);
                break;
        }
    }

    if (!err)
        err = finish_data(&data);
    if (err)
        printf("Error \n");

    return err;
}
