#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "utils.h"

int main()
{
    char string[MAX_STRING_LENGHT + 1] = { 0 };
    char accept[MAX_STRING_LENGHT + 1] = { 0 };

    int input_error = 0, error = 0;


    input_error = input_string(string);

    if (!input_error)
        input_error = input_string(accept);


    if (!input_error)
    {
        size_t std_res = strspn(string, accept);
        size_t my_res = my_strspn(string, accept); 
        error = std_res != my_res;

        printf("INPUT DATA:\n%s\n%s\n", string, accept);
        printf("STD func return %lu\n", std_res);
        printf("MY  func return %lu\n", my_res);
    }
    

    if (error)
        printf("NEGATIVE check\n");
    else if (input_error)
        printf("Input error\n");
    else
        printf("OK\n");

    
    return error || input_error;
}
