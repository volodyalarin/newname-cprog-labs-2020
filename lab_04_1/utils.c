#include <stdio.h>
#include "utils.h"

int input_string(char *str)
{
    size_t i;
    char cur = getchar();

    for (i = 0; i < MAX_STRING_LENGHT && cur > 0 && cur != '\n' && cur != -1; i++)
    {
        str[i] = cur;
        cur = getchar();
    }

    str[i] = '\0';

    return cur != '\n' && cur != -1;    
}

size_t my_strspn(const char *s, const char *accept)
{
    size_t i = 0;
    while (s[i] != '\0' && find_str_char(s[i], accept)) 
        i++;
    return i;
}

char *find_str_char(const char ch, const char *accept)
{
    while (*accept)
    {
        if (*accept == ch)
            return (char*) accept;
        accept++;
    }

    return 0;
}
