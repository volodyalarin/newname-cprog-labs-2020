#ifndef _UNTILS_H_
#define _UNTILS_H_

#include <stdlib.h>
#include <stdbool.h>

#define POINTER_ERROR -1 

#define MAX_STRING_LENGHT 1023

int input_string(char *str);

size_t my_strspn(const char *s, const char *accept);

char *find_str_char(const char ch, const char *accept);


#endif // _UNTILS_H_
