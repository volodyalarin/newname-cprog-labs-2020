#include <stdio.h>
#include "untils.h"

int main(void)
{
    int err = 0;

    long long array[MAX_ARRAY_SIZE] = { 0 };
    int length = 0;

    long long computed[MAX_COMPUTED_ARRAY_SIZE] = { 0 };
    int computed_length = 0;

    if (!err)
        err = input_array(array, &length);

    if (!err)
        err = generate_new_array(array, length, computed, &computed_length);

    if (!err)
        err = computed_length == 0;

    if (!err)
        output_array(computed, computed_length);
    else
        printf("error \n");
    return err;
}
