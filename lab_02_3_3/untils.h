#ifndef __UNTILS__H__
#define __UNTILS__H__

#define INPUT_ERROR -1
#define POINTER_ERROR -2

#define INPUT_SUCCESS 0

#define MAX_ARRAY_SIZE 10
#define MAX_COMPUTED_ARRAY_SIZE MAX_ARRAY_SIZE*2


int input_array(long long *array, int *length);
int output_array(long long *array, int length);


int generate_new_array(const long long *array, const int length, long long *generated, int *generated_length);

int insert_array(long long element, int position, long long *array, int length);
int copy_array(long long *dest_array, const long long *source_array, const int length);

long long reverse_number(long long number);
#endif // __UNTILS__H__
