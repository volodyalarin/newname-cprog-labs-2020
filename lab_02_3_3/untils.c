#include <stdio.h>
#include "untils.h"

int input_array(long long *array, int *length)
{
    if (!array || !length)
        return POINTER_ERROR;
    
    int err = INPUT_SUCCESS;

    printf("Input length of array: ");
    if (scanf("%d", length) != 1 || *length <= 0 || *length > MAX_ARRAY_SIZE)
        err = INPUT_ERROR;
    if (!err)
        printf("Input array elements: ");
    
    for (int i = 0; i < (*length) && !err; i++)
    {
        if (scanf("%lld", array + i) != 1)
            err = INPUT_ERROR; 
    }
    
    return err;
}

int output_array(long long *array, int length)
{
    if (!array)
        return POINTER_ERROR;
    
    printf("Your array is \n");
    for (int i = 0; i < length; i++)
        printf("%lld ", array[i]);
    printf("\n");

    return 0;
}


int generate_new_array(const long long *array, const int length, long long *generated, int *generated_length)
{
    int err = copy_array(generated, array, length);
    
    if (!err)
    {
        *generated_length = length;
        for (int i = 0; i < *generated_length; i++)
        {
            if (generated[i] > 0)
            {
                insert_array(reverse_number(generated[i]), i + 1, generated, *generated_length);
                (*generated_length)++;
                i++;
            }
        }
    }

    return err;
}

int insert_array(long long element, int position, long long *array, int length)
{
    if (!array)
        return POINTER_ERROR;
    
    for (int i = length; i > position; i--)
        array[i] = array[i - 1];
    
    array[position] = element;

    return 1;
}

int copy_array(long long *dest_array, const long long *source_array, const int length)
{
    if (!dest_array || !source_array)
        return POINTER_ERROR;

    for (int i = 0; i < length; i++)
        dest_array[i] = source_array[i];
    
    return 0;
}

long long reverse_number(long long number)
{
    long long result = 0;

    while (number > 0)
    {
        result *= 10;
        result += number % 10;
        number /= 10;
    }

    return result;
}