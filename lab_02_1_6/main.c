#include <stdio.h>
#include "untils.h"

int main(void)
{
    int err = 0;

    
    size_t repeats_count[] = {10, 100, 500, 1000};
    size_t sizes[] = {10, 100, 500, 1000};

    long long process_1_array[MAX_ARRAY_SIZE] = { 0 };
    long long process_2_array[MAX_ARRAY_SIZE] = { 0 };
    long long process_3_array[MAX_ARRAY_SIZE] = { 0 };

    init_random_generator();

    print_table_header();

    for (size_t i = 0; i < sizeof(repeats_count) / sizeof(*repeats_count) && !err; i++)
        for (size_t j = 0; j < sizeof(sizes) / sizeof(*sizes) && !err; j++)
        {  
            size_t repeats = repeats_count[i];
            size_t size = sizes[j];

            long long process_1_time = 0, process_2_time = 0, process_3_time = 0;

            for (size_t k = 0; k < repeats && !err; k++)
            {
                random_array(process_1_array, size);
                copy_array(process_2_array, process_1_array, size);
                copy_array(process_3_array, process_1_array, size); 
                
                long long current_time_1 = 0, current_time_2 = 0, current_time_3 = 0;

                err = test_process_1(process_1_array, size, &current_time_1);
                if(!err)
                    err = test_process_2(process_2_array, size, &current_time_2);
                if(!err)
                    err = test_process_3(process_3_array, size, &current_time_3);
                
                process_1_time += current_time_1;
                process_2_time += current_time_2;
                process_3_time += current_time_3;
            }

            print_table_row(repeats, size, process_1_time, process_2_time, process_3_time);
        }
                
    return err;
}
