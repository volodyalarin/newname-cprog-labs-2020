#ifndef __UNTILS__H__
#define __UNTILS__H__

#define INPUT_ERROR -1
#define POINTER_ERROR -2

#define INPUT_SUCCESS 0

#define MAX_ARRAY_SIZE 1000

void init_random_generator();
int random_array(long long *array, size_t length);

int process_1(long long *array, int length, long long *result);
int process_2(long long *array, int length, long long *result);
int process_3(long long *array, long long *end, long long *result);

int test_process_1(long long *array, int length, long long *time);
int test_process_2(long long *array, int length, long long *time);
int test_process_3(long long *array, int size, long long *time);

void print_table_header();
void print_table_row(size_t repeats, size_t size, long long time_1, long long time_2, long long time_3);
int copy_array(long long *dest_array, const long long *source_array, const int length);

int output_array(long long *array, int length);

#endif // __UNTILS__H__
