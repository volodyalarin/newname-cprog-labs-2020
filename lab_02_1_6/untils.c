#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "untils.h"

int copy_array(long long *dest_array, const long long *source_array, const int length)
{
    if (!dest_array || !source_array)
        return POINTER_ERROR;

    for (int i = 0; i < length; i++)
        dest_array[i] = source_array[i];
    
    return 0;
}

void init_random_generator()
{
    struct timeval time = { 0 };
    gettimeofday(&time, 0);
    srand(time.tv_sec);
}


int random_array(long long *array, size_t length)
{
    if (!array)
        return POINTER_ERROR;
    
    int err = 0;

    for (size_t i = 0; i < length; i++)
        array[i] = rand(); 

    return err;
}

int process_1(long long *array, int length, long long *result)
{
    if (!array || !result)
        return POINTER_ERROR;
    
    *result = 0;

    long long i = 0;
    long long j = length - 1;

    while (i < length && j >= 0)
    {        
        while (array[i] >= 0 && i < length) 
            i++;
        while (array[j] <= 0 && j >= 0) 
            j--;
        
        if (i < length && j >= 0)
            *result += array[i] * array[j];

        i++;
        j--;
    }
    
    return *result == 0;
}

int process_2(long long *array, int length, long long *result)
{
    if (!array || !result)
        return POINTER_ERROR;
    
    *result = 0;

    long long i = 0;
    long long j = length - 1;

    while (i < length && j >= 0)
    {        
        while (*(array + i) >= 0 && i < length) 
            i++;
        while (*(array + j) <= 0 && j >= 0) 
            j--;
        
        if (i < length && j >= 0)
            *result += *(array + i) * *(array + j);
            
        i++;
        j--;
    }
    
    return *result == 0;
}

int process_3(long long *array, long long *end, long long *result)
{
    if (!array || !end || !result)
        return POINTER_ERROR;
    
    *result = 0;

    long long *pi = array;
    long long *pj = end - 1;

    while (pi < end && pj >= array)
    {        
        while (*pi >= 0 && pi < end) 
            pi++;
        while (*pj <= 0 && pj > array) 
            pj--;
        
        if (pi < end && pj > array)
            *result += (*pi) * (*pj);
        
        pi++;
        pj--;
    }
    
    return *result == 0;
}

long long elapsed_time(struct timeval time_start, struct timeval time_stop)
{
    return 
        (time_stop.tv_sec - time_start.tv_sec) * 1000000LL +
        (time_stop.tv_usec - time_start.tv_usec);
}

int test_process_1(long long *array, int length, long long *time)
{
    struct timeval time_start, time_end;
    gettimeofday(&time_start, 0);

    long long result = 0;
    process_1(array, length, &result);

    gettimeofday(&time_end, 0);
    *time = elapsed_time(time_start, time_end);
    return 0;  
}
int test_process_2(long long *array, int length, long long *time)
{
    struct timeval time_start, time_end;
    gettimeofday(&time_start, 0);

    long long result = 0;
    process_2(array, length, &result);

    gettimeofday(&time_end, 0);
    *time = elapsed_time(time_start, time_end);
    return 0;
}
int test_process_3(long long *array, int size, long long *time)
{
    struct timeval time_start, time_end;
    long long *end = array + size;

    gettimeofday(&time_start, 0);
    
    long long result = 0;
    process_3(array, end, &result);

    gettimeofday(&time_end, 0);
    *time = elapsed_time(time_start, time_end);
    return 0;
}

void print_table_header()
{
    printf("|%7s|%7s|%7s|%7s|%7s|\n", "Repeats", "Size", "a[i]", "*(a+i)", "Pointer");
    printf("|-------|-------|-------|-------|-------|\n");
}

void print_table_row(size_t repeats, size_t size, long long time_1, long long time_2, long long time_3)
{
    printf("|%7ld|%7ld|%7lld|%7lld|%7lld|\n", repeats, size, time_1, time_2, time_3);
}


int output_array(long long *array, int length)
{
    if (!array)
        return POINTER_ERROR;
    
    printf("Your array is \n");
    for (int i = 0; i < length; i++)
        printf("%lld ", array[i]);
    printf("\n");

    return 0;
}
