#ifndef __UNTILS__H__
#define __UNTILS__H__

#include <stdlib.h>
#include <stdbool.h>
#include "matrix.h"

#define POINTER_ERROR -1 

int process_matrix(long long **matrix, size_t rows, size_t cols, long long *result);
int is_monotonic_sequence(const long long *array, size_t length, bool *res);
int is_preserves_sequence(const long long *array, size_t length, bool *res);
int is_reverses_sequence(const long long *array, size_t length, bool *res);

#endif // __UNTILS__H__
