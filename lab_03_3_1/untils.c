#include "untils.h"

int process_matrix(long long **matrix, size_t rows, size_t cols, long long *result)
{
    int err = 0;
    for (size_t i = 0; i < rows && !err; i++)
    {
        bool res;
        err = is_monotonic_sequence(matrix[i], cols, &res);
        if (!err)
            result[i] = res ? 1 : 0;
    }

    return err;    
}

int is_monotonic_sequence(const long long *array, size_t length, bool *res)
{
    bool is_reverses, is_preserves;
    
    int err = is_preserves_sequence(array, length, &is_preserves);
    if (!err)
        err = is_reverses_sequence(array, length, &is_reverses);
    
    *res = is_reverses || is_preserves;

    return err;
}

int is_preserves_sequence(const long long *array, size_t length, bool *res)
{
    if (!array || !res)
        return POINTER_ERROR;
    
    *res = length > 1;

    for (size_t i = 1; i < length && *res; i++)
    {
        *res = array[i - 1] <= array[i];
    }

    return 0;
}

int is_reverses_sequence(const long long *array, size_t length, bool *res)
{
    if (!array || !res)
        return POINTER_ERROR;
    
    *res = length > 1;

    for (size_t i = 1; i < length && *res; i++)
    {
        *res = array[i - 1] >= array[i];
    }

    return 0;
}