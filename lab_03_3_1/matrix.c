#include <stdio.h>

#include "matrix.h"

void transform(long long **matrix, long long buffer[][MAX_MATRIX_COLS])
{
    for (size_t i = 0; i < MAX_MATRIX_ROWS; i++)
        matrix[i] = buffer[i];
}

int input_matrix_size(size_t *rows, size_t *cols)
{
    if (!rows || !cols)
        return POINTER_ERROR;

    printf("Input rows and cols: ");
    return scanf("%lu%lu", rows, cols) != 2
        || *rows <= 0 || *rows > MAX_MATRIX_ROWS
        || *cols <= 0 || *cols > MAX_MATRIX_COLS;
}

int input_array(long long *array, size_t length)
{
    if (!array)
        return POINTER_ERROR;
        
    printf("Input elements: ");
    
    int error = 0;

    for (size_t i = 0; i < length && !error; i++)
    {
        error = scanf("%lld", &array[i]) != 1;
    }
    return error;
}

int input_matrix(long long **matrix, size_t rows, size_t cols)
{
    if (!matrix)
        return POINTER_ERROR;
    
    int error = 0;

    for (size_t i = 0; i < rows && !error; i++)
    {
        error = input_array(matrix[i], cols);
    }
    return error;
}

int output_matrix(long long **matrix, size_t rows, size_t cols)
{
    if (!matrix)
        return POINTER_ERROR;
    
    for (size_t i = 0; i < rows; i++)
    {
        output_array(matrix[i], cols);
    }
    
    return 0;
}

int output_array(const long long *array, size_t length)
{
    if (!array)
        return POINTER_ERROR;
    
    for (size_t i = 0; i < length; i++)
    {
        printf("%lld ", array[i]);
    }
    printf("\n");
    
    return 0;
}
