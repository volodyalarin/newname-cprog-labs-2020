// По матрице получить одномерный массив, присвоив его k-тому элементу значение 1, если
// выполняется указанное ниже условие, и значение 0 иначе:
// элементы k-ой строки образуют монотонную последовательность.
#include "matrix.h"
#include "untils.h"

#include "stdlib.h"
#include "stdio.h"

int main()
{
    long long buffer [MAX_MATRIX_ROWS][MAX_MATRIX_COLS] = { { 0 } };
    long long *matrix[MAX_MATRIX_ROWS];
    transform(matrix, buffer);
    
    long long result [MAX_MATRIX_ROWS];
    int error = 0;
    
    size_t rows, cols = 0;

    error = input_matrix_size(&rows, &cols);

    if (!error)
        error = input_matrix(matrix, rows, cols);

    if (!error)
        error = process_matrix(matrix, rows, cols, result);

    if (!error)
        error = output_array(result, rows);    

    if (error)
        printf("Error \n");
    
    return error;
}
